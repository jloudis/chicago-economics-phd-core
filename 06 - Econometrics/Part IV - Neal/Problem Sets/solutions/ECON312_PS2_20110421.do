*** Parts of this code are based on a problem set solution by Juliette Caminade ***


clear 
capture log close
set more off

cd "D:\Eigene Dateien\PhD Chicago\3rd year\ECON312_TA"


set mem 1200m

log using ECON312_PS.log, replace

foreach i in 1980 2009{

use ECON312_PS_data.dta, clear
keep if year == `i'


************************
*** Data preparation ***
************************

drop if incwage == 9999999
drop if ftotinc == 9999999

* Generate hourly wages 

gen weeks = 0 if wkswork2==0
replace weeks = 7 if wkswork2==1
replace weeks = 20 if wkswork2==2
replace weeks = 33 if wkswork2==3
replace weeks = 43.5 if wkswork2==4
replace weeks = 48.5 if wkswork2==5
replace weeks = 51 if wkswork2==6

gen total_hours = weeks*uhrswork
replace total_hours =. if total_hours == 0

gen ln_w = log(incwage/total_hours)
gen employed = (ln_w != .)


* Generate X-buckets

gen age1 = (age <= 31)
gen age2 = (age >= 32 & age <= 37)
gen age3 = (age >= 38 & age <= 43)
gen age4 = (age >= 44 & age <= 49)
 
gen education1 = (educ <6)
gen education2 = (educ == 6)
gen education3 = (educ  >= 7 & educ <=9)
gen education4 = (educ == 10| educ == 11)


foreach age in age1 age2 age3 age4{
	foreach educ in education1 education2 education3 education4{
		gen `educ'_`age' = (`educ'==1 & `age'==1)
	}
}

egen X_bucket = group(education1_age1-education4_age4)

global X " education2_age1 education3_age1 education4_age1 education1_age2- education4_age4"

* Generate the Z buckets

gen inc_unearned = ftotinc - incwage

sum inc_unearned, d

gen inc_unearned_p0 = (inc_unearned<=r(p25))
gen inc_unearned_p25 = (inc_unearned>r(p25) & inc_unearned<=r(p50))
gen inc_unearned_p50 = (inc_unearned>r(p50) & inc_unearned<=r(p75))
gen inc_unearned_p75 = (inc_unearned>r(p75))


gen child0 = (nchild==0)
gen child1 = (nchild==1)
gen child2 = (nchild==2)
gen child3 = (nchild>=3)

foreach i  in  inc_unearned_p0 inc_unearned_p25 inc_unearned_p50 inc_unearned_p75 {
	foreach j in child0 child1 child2 child3{
		gen `i'_`j' = (`i'==1 & `j'==1)
	}
}

egen Z_bucket = group(inc_unearned_p0_child0-inc_unearned_p75_child3)

global Z "inc_unearned_p25 - inc_unearned_p75 child1-child3"


*** part a) ***
***************

* 1st stage: 
probit employed $X $Z [weight = perwt]
predict xbeta_hat, xb
gen lambda_hat = normalden(-xbeta_hat)/(1-normal(-xbeta_hat))
*gen anti_lambda = -normalden(proba)/normal(-proba)

* 2nd stage
* the prediction is Xb + gamma*lamda
reg ln_w $X lambda_hat [weight = perwt]
predict wage_latent_a, xb

replace wage_latent_a = wage_latent_a - _coef[lambda_hat]*lambda_hat


/*
collapse latentwage $w8, by(bucket) 

matrix index =.
matrix A=.
foreach i of numlist 1/12{
matrix index=index \ `i'
matrix A=A\latentwage[`i']
}
 */

*** part b) ***
***************

* calculate propensity score

bysort X_bucket Z_bucket: egen sum_perwt_bucket = total(perwt)
bysort X_bucket Z_bucket: egen pscore_1 = total(employed*perwt/sum_perwt_bucket)

forvalue i = 2/4{
	gen pscore_`i' = pscore_1^`i' 
}



regress ln_w $X pscore_1-pscore_4 if employed == 1 [weight = perwt]
predict wage_latent_b, xb

* use identification at infinity to get the constant
gen alpha_hat = ln_w - wage_latent_b + _coef[_cons] + _coef[pscore_1]*pscore_1 + _coef[pscore_2]*pscore_2 + _coef[pscore_3]*pscore_3 + _coef[pscore_4]*pscore_4 if X_bucket == 13 & Z_bucket == 16
egen constant = mean(alpha_hat)
display "constant"
sum constant

replace wage_latent_b = wage_latent_b + constant - _coef[_cons] - _coef[pscore_1]*pscore_1 - _coef[pscore_2]*pscore_2- _coef[pscore_3]*pscore_3- _coef[pscore_4]*pscore_4

sum wage_latent_a wage_latent_b, d

** part c) ***

drop pscore*

*calculate P(X)
bysort X_bucket: egen NX=sum(perwt)
bysort X_bucket: egen working=sum(perwt) if employed == 1

gen p = working/NX



gen L_percentile =  100*max((p-.5)/p,0.0001)
gen H_percentile = 100*min(.5/p,0.9999)

gsort -X_bucket


matrix LW=.
matrix ML=.
matrix MH=.


*Computation of the bounds, w_low and w_high
foreach j of numlist 1/16{
sum L_percentile if X_bucket==`j'
_pctile  ln_w if employed==1 & X_bucket==`j' [fweight=perwt] , percentiles(`r(mean)')
matrix ML=ML\r(r1)


sum H_percentile if X_bucket==`j'
_pctile  ln_w if employed == 1 & X_bucket == `j'  [fweight=perwt], percentiles(`r(mean)')
matrix MH=MH\r(r1)

_pctile wage_latent_a if X_bucket==`j' [fweight=perwt],percentiles(50)
matrix LW=LW\r(r1)


}

matrix Results = ML,LW,MH
matrix list Results
outtable using Manski_`i' , mat(Results) nobox replace

}

log close




